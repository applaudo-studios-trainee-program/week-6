const BASE_URL = process.env.REACT_APP_BASE_URL;

interface IClientProps {
  endpoint: string;
  data?: {
    [key: string]: string;
  };
  method?: 'GET' | 'POST' | 'PUT' | 'DELETE';
}

export const fetchDataWithoutJWT = ({
  endpoint,
  data = {},
  method = 'GET',
}: IClientProps) => {
  const URL = `${BASE_URL}${endpoint}`;

  if (method === 'GET') return fetch(URL);

  return fetch(URL, {
    method,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });
};

interface IFetchWJWT extends IClientProps {
  token: string;
}

export const fetchDataWithJWT = ({
  endpoint,
  data,
  token,
  method = 'GET',
}: IFetchWJWT) => {
  const URL = `${BASE_URL}${endpoint}`;

  if (method === 'GET') {
    return fetch(URL, {
      method,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  }

  return fetch(URL, {
    method,
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });
};
