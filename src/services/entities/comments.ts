import { fetchDataWithJWT } from 'services/client';

type PostCommentProps = { body: string; gameId: string; token: string };

export const postComment = async ({
  body,
  gameId,
  token,
}: PostCommentProps) => {
  await fetchDataWithJWT({
    endpoint: `games/${gameId}/comment`,
    data: { body },
    method: 'POST',
    token,
  });
};
