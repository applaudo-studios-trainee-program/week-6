import { fetchDataWithoutJWT } from 'services/client';
import { IUser } from 'models/userModels';

const ENDPOINT = 'auth';

export interface IErrorMessage {
  id: string;
  message: string;
}

export interface ISessionData {
  jwt: string;
  user: IUser;
}

interface IBadRequest {
  statusCode: number;
  message: { messages: IErrorMessage[] }[];
}

type PostUserProps = {
  identifier: string;
  password: string;
};

export const postUser = async ({ identifier, password }: PostUserProps) => {
  const response = await fetchDataWithoutJWT({
    endpoint: `${ENDPOINT}/local`,
    data: { identifier, password },
    method: 'POST',
  });

  if (response.status !== 200) {
    const body: IBadRequest = await response.json();

    const errorMessages = body.message
      .map((messageList) => messageList.messages)
      .map((message) => message)
      .flat();

    return { error: true, errorMessages };
  }

  const body: ISessionData = await response.json();

  const user: IUser = {
    firstName: body.user.firstName,
    lastName: body.user.lastName,
    id: body.user.id,
    username: body.user.username,
  };

  return { error: false, jwt: body.jwt, user };
};
