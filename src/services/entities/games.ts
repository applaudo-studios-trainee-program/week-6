import { fetchDataWithoutJWT } from 'services/client';
import {
  IComment,
  ICoverArt,
  IGenre,
  IMetaInfo,
  IPlatform,
  IPublisher,
} from 'models/gamesModels';

const ENDPOINT = 'games';

export const getGamesCount = async () => {
  const response = await fetchDataWithoutJWT({ endpoint: `${ENDPOINT}/count` });

  const gamesCount: number = await response.json();

  return gamesCount;
};

interface IGetGamesResponse {
  id: string;
  name: string;
  publishers: IPublisher[];
  price: number;
  cover_art: ICoverArt;
}

type GetGamesProps = {
  offset: number;
  searchEndpoint: string;
  actualPage?: number;
};

export const getGames = async ({
  offset,
  actualPage = 0,
  searchEndpoint,
}: GetGamesProps) => {
  const response = await fetchDataWithoutJWT({
    endpoint: `${ENDPOINT}?_limit=${offset}&_start=${
      offset * actualPage
    }${searchEndpoint}`,
  });

  const gamesData: IGetGamesResponse[] = await response.json();

  return gamesData.map((gameData) => {
    const { id, name, price, cover_art, publishers: publisherList } = gameData;

    const publishers: IPublisher[] = publisherList.map((publisher) => {
      const { id, name } = publisher;

      return { id, name };
    });

    const coverArt: ICoverArt = {
      url:
        cover_art?.url ||
        'https://oij.org/wp-content/uploads/2016/05/placeholder.png',
    };

    return { id, name, price, coverArt, publishers };
  });
};

interface IGetGameByIdResponse {
  name: string;
  price: number;
  publishers: IPublisher[];
  platforms: IPlatform[];
  comments: IComment[];
  genre: IGenre;
  cover_art: ICoverArt;
  release_year: number;
}

type GetGameByIdProps = {
  gameId: string;
};

export const getGameById = async ({ gameId }: GetGameByIdProps) => {
  const response = await fetchDataWithoutJWT({
    endpoint: `${ENDPOINT}/${gameId}`,
  });

  const gameData: IGetGameByIdResponse = await response.json();

  const { name, price } = gameData;

  const coverArt: ICoverArt = {
    url:
      gameData.cover_art?.url ||
      'https://oij.org/wp-content/uploads/2016/05/placeholder.png',
  };

  const publishers: IPublisher[] = gameData.publishers.map((publisher) => {
    const { id, name } = publisher;

    return { id, name };
  });

  const platforms: IPlatform[] = gameData.platforms.map((platform) => {
    const { id, name } = platform;

    return { id, name };
  });

  const releaseYear = gameData.release_year;

  const metaInfo: IMetaInfo[] = [
    { id: 0, title: 'Price', content: `$ ${price} USD` },
    { id: 1, title: 'Release Year', content: `${releaseYear}` },
    { id: 2, title: 'Genre', content: gameData.genre.name.toUpperCase() },
    { id: 3, title: 'Publishers', content: publishers },
    { id: 4, title: 'Platforms', content: platforms },
  ];

  return { name, coverArt, metaInfo };
};

type GetGameCommentsProps = {
  gameId: string;
};

export const getGameComments = async ({ gameId }: GetGameCommentsProps) => {
  const response = await fetchDataWithoutJWT({
    endpoint: `${ENDPOINT}/${gameId}/comments`,
  });

  const comments: IComment[] = await response.json();

  return comments.map((comment) => {
    const { id, body, user: userData } = comment;

    const { id: userId, firstName, lastName } = userData;

    return { id, body, user: { id: userId, firstName, lastName } };
  });
};

interface IGetGamesBySearchQueryResponse {
  id: string;
  name: string;
  publishers: IPublisher[];
  price: number;
  cover_art: ICoverArt;
}

type GetGamesBySearchQueryProps = { searchQuery: string };

export const getGamesBySearchQuery = async ({
  searchQuery,
}: GetGamesBySearchQueryProps) => {
  const response = await fetchDataWithoutJWT({
    endpoint: `${ENDPOINT}?name_contains=${searchQuery}&_sort=name`,
  });

  const gamesData: IGetGamesBySearchQueryResponse[] = await response.json();

  return gamesData.map((gameData) => {
    const { id, name, cover_art } = gameData;

    const coverArt: ICoverArt = {
      url:
        cover_art?.url ||
        'https://oij.org/wp-content/uploads/2016/05/placeholder.png',
    };

    return { id, name, coverArt };
  });
};
