import { AddError, RemoveError } from 'models/errorActionsTypes';

interface IError {
  [key: string]: { message: string };
}

type Actions = AddError | RemoveError;

export const errorReducer = (state: IError, action: Actions) => {
  switch (action.type) {
    case 'ADD':
      return {
        ...state,
        [action.payload.inputName]: { message: action.payload.message },
      };
    case 'REMOVE': {
      const { [action.payload.inputName]: omitted, ...rest } = state;

      return rest;
    }

    default:
      return state;
  }
};
