export interface IGenre {
  id: string;
  name: string;
}

export interface IPlatform {
  id: string;
  name: string;
}

export interface IPublisher {
  id: string;
  name: string;
}

export interface ICoverArt {
  url: string;
}

export interface IComment {
  id: string;
  body: string;
  user: { id: number; firstName: string; lastName: string };
}

export interface IMetaInfo {
  id: number;
  title: string;
  content: string | IPlatform[] | IPublisher[];
}

export interface IGamesData {
  id: string;
  name: string;
  price: number;
  coverArt: ICoverArt;
  publishers: IPublisher[];
}

export interface ISmallGamesData {
  id: string;
  name: string;
  coverArt: ICoverArt;
}
