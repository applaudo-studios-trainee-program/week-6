const ADD_ERROR = 'ADD';
const REMOVE_ERROR = 'REMOVE';

export interface IError {
  inputName: string;
  message: string;
}

export interface AddError {
  type: typeof ADD_ERROR;
  payload: { inputName: string; message: string };
}

export interface RemoveError {
  type: typeof REMOVE_ERROR;
  payload: { inputName: string };
}
