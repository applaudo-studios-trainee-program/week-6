import { FC } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import { ISessionData } from 'services/entities/auth';
import { useLocalStorage } from 'hooks/useLocalStorage';
import Auth from 'views/Auth/Auth';
import Catalog from 'views/Catalog/Catalog';
import Footer from 'components/UI/Footer/Footer';
import Game from 'views/Game/Game';
import Home from 'views/Home/Home';
import Navbar from 'components/UI/Navbar/Navbar';
import PrivateRoute from 'routers/PrivateRoute';
import Search from 'views/Search/Search';

enum Paths {
  HomePath = '/',
  AuthPath = '/auth',
  CatalogPath = '/catalog',
  SearchPath = '/search',
  GamePath = '/game/:id',
}

const AppRouter: FC = () => {
  const { storedValue, setValue } = useLocalStorage<ISessionData>({
    key: 'sessionData',
    initialValue: {
      jwt: '',
      user: { id: 0, username: '', firstName: '', lastName: '' },
    },
  });

  const { user } = storedValue;

  return (
    <Router>
      <header
        style={{
          backgroundColor: '#262828',
          position: 'sticky',
          top: 0,
          zIndex: 999,
        }}
      >
        <Navbar userData={user} setStorage={setValue} />
      </header>

      <main style={{ backgroundColor: '#0e1111', height: '100%' }}>
        <Switch>
          <Route exact path={Paths.HomePath} component={Home} />
          <Route
            path={Paths.AuthPath}
            component={() => <Auth setStorage={setValue} />}
          />
          <Route path={Paths.CatalogPath} component={Catalog} />
          <Route path={Paths.SearchPath} component={Search} />

          <PrivateRoute
            isAuthenticated={user.username !== '' && true}
            component={Game}
            path={Paths.GamePath}
          />

          <Redirect to={Paths.HomePath} />
        </Switch>
      </main>

      <Footer />
    </Router>
  );
};

export default AppRouter;
