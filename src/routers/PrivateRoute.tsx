import { FC } from 'react';
import { Redirect, Route } from 'react-router-dom';

type Props = {
  isAuthenticated: boolean;
  component: FC;
  path: string;
  exact?: boolean;
};

const PrivateRoute: FC<Props> = ({
  isAuthenticated,
  component: Component,
  path,
  exact,
}) => (
  <Route
    path={path}
    exact={exact || false}
    component={() =>
      isAuthenticated ? <Component /> : <Redirect to="/auth" />
    }
  />
);

export default PrivateRoute;
