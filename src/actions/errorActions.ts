import { AddError, RemoveError } from 'models/errorActionsTypes';

type AddErrorType = ({
  inputName,
  message,
}: {
  inputName: string;
  message: string;
}) => AddError;

export const addError: AddErrorType = ({ inputName, message }) => ({
  type: 'ADD',
  payload: { inputName, message },
});

type RemoveErrorType = ({ inputName }: { inputName: string }) => RemoveError;

export const removeError: RemoveErrorType = ({ inputName }) => ({
  type: 'REMOVE',
  payload: { inputName },
});
