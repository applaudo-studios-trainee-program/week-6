import { useState } from 'react';

type Props<T> = { key: string; initialValue: T };

export const useLocalStorage = <T>({ key, initialValue }: Props<T>) => {
  const [storedValue, setStoredValue] = useState<T>(() => {
    const item = localStorage.getItem(key);

    return item ? JSON.parse(item) : initialValue;
  });

  const setValue = (value: T | ((val: T) => T)) => {
    const valueToStore = value instanceof Function ? value(storedValue) : value;

    setStoredValue(valueToStore);

    localStorage.setItem(key, JSON.stringify(valueToStore));
  };

  return { storedValue, setValue };
};
