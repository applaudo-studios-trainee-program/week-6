import { ChangeEvent, useState } from 'react';

type UseAuthType<T> = {
  ({ initialFormState }: { initialFormState: T }): [
    T,
    ({ target }: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void,
    () => void
  ];
};

export const useAuth: UseAuthType<{ [key: string]: string }> = ({
  initialFormState,
}) => {
  const [formState, setFormState] = useState(initialFormState);

  const handleChange = ({
    target,
  }: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const input = target;

    setFormState((state) => ({ ...state, [input.name]: input.value }));
  };

  const resetValues = () => setFormState(initialFormState);

  return [formState, handleChange, resetValues];
};
