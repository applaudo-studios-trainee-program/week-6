import { useEffect, useRef, useState } from 'react';

import { getGames, getGamesCount } from 'services/entities/games';
import { IGamesData } from 'models/gamesModels';

type Props = { itemsPerPage: number; searchEndpoint: string };

export const usePagination = ({ itemsPerPage, searchEndpoint }: Props) => {
  const isMounted = useRef(false);

  const [itemsToShow, setItemsToShow] = useState<IGamesData[]>([]);
  const [actualPage, setActualPage] = useState(0);
  const [totalPages, setTotalPages] = useState(0);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  }, []);

  useEffect(() => {
    getGamesCount().then((response) => {
      if (isMounted.current)
        setTotalPages(Math.ceil(response / itemsPerPage) - 1);
    });
  }, []);

  useEffect(() => {
    getGames({
      offset: itemsPerPage,
      actualPage,
      searchEndpoint,
    }).then((response) => {
      if (isMounted.current) setItemsToShow(response);
    });
  }, [actualPage]);

  return { itemsToShow, actualPage, totalPages, setActualPage };
};
