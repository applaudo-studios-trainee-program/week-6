import { FC, FormEvent, useState } from 'react';
import validator from 'validator';

import { useAuth } from 'hooks/useAuth';
import Button from 'components/Button/Button';
import Styles from './CommentMaker.module.css';

type Props = { onSubmitFn: ({ comment }: { comment: string }) => void };

const CommentMaker: FC<Props> = ({ onSubmitFn }) => {
  const [errorOnSubmit, setErrorOnSubmit] = useState({ message: '' });

  const [formState, handleChange, resetForm] = useAuth({
    initialFormState: { comment: '' },
  });

  const { comment } = formState;

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (validator.isEmpty(comment.trim())) {
      setErrorOnSubmit({ message: "This input can't be empty" });
    } else {
      setErrorOnSubmit({ message: '' });

      onSubmitFn({ comment });

      resetForm();
    }
  };

  return (
    <div className={Styles.container}>
      <form onSubmit={handleSubmit}>
        <label className={Styles['form-group']} htmlFor="comment">
          Write a comment
          <textarea
            className={Styles['form-input']}
            id="comment"
            name="comment"
            value={comment}
            onChange={handleChange}
          />
          {errorOnSubmit.message !== '' && (
            <p className={Styles['error-message']}>
              <small>{errorOnSubmit.message}</small>
            </p>
          )}
        </label>

        <Button content="Submit" type="submit" />
      </form>
    </div>
  );
};

export default CommentMaker;
