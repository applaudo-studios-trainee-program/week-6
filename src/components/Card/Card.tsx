import { FC } from 'react';

import { ICoverArt, IPublisher } from 'models/gamesModels';
import Styles from './Card.module.css';

type Props = {
  name: string;
  price: number;
  publishers: IPublisher[];
  coverArt: ICoverArt;
};

const Card: FC<Props> = ({ name, price, publishers, coverArt }) => {
  const { url } = coverArt;

  return (
    <div className={Styles.container}>
      <img
        className={Styles.image}
        src={url}
        alt={`${name} Cover`}
        title={`${name} Cover`}
      />

      <section className={Styles.content}>
        <p className={Styles.name}>{name}</p>

        {publishers.map((publisher) => (
          <p key={publisher.id} className={Styles['publisher-name']}>
            {publisher.name}
          </p>
        ))}

        <p className={Styles.price}>$ {price} USD</p>
      </section>
    </div>
  );
};

export default Card;
