import { FC } from 'react';

import Styles from './Social.module.css';

type Props = { socialIcon: FC<{ className: string }>; username: string };

const Social: FC<Props> = ({ socialIcon: SocialIcon, username }) => (
  <div className={Styles.container}>
    <SocialIcon className={Styles.icon} />
    <p className={Styles.username}>{username}</p>
  </div>
);

export default Social;
