import { FC } from 'react';

import AppRouter from 'routers/AppRouter';

const App: FC = () => <AppRouter />;

export default App;
