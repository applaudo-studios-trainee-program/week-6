import { FC, memo } from 'react';

import { IPlatform, IPublisher } from 'models/gamesModels';
import Styles from './MetaInfo.module.css';

type Props = { title: string; content: string | IPublisher[] | IPlatform[] };

const MetaInfo: FC<Props> = ({ title, content }) => (
  <div className={Styles.container}>
    <h4 className={Styles.title}>{title}</h4>

    <p className={Styles.content}>
      {typeof content !== 'object'
        ? content
        : content.map((data, index) => {
            if (!index) return `${data.name}`;

            return `, ${data.name}`;
          })}
    </p>
  </div>
);

export default memo(MetaInfo);
