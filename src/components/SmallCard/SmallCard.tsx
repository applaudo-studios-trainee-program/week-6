import { FC } from 'react';

import { ICoverArt } from 'models/gamesModels';
import Styles from './SmallCard.module.css';

type Props = { name: string; coverArt: ICoverArt };

const SmallCard: FC<Props> = ({ name, coverArt }) => {
  const { url } = coverArt;

  return (
    <div className={Styles.container}>
      <img
        className={Styles.image}
        src={url}
        alt={`${name} Cover`}
        title={`${name} Cover`}
      />
    </div>
  );
};

export default SmallCard;
