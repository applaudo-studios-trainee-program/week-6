import { FC, memo } from 'react';
import { Link } from 'react-router-dom';

import Facebook from 'assets/Icons/Facebook';
import Instagram from 'assets/Icons/Instagram';
import Social from 'components/Social/Social';
import Styles from './Footer.module.css';

const Footer: FC = () => (
  <footer className={Styles.container}>
    <h3 className={Styles.title}>React Stack</h3>

    <section className={Styles.section}>
      <h4 className={Styles['section-title']}>Social</h4>

      <Social socialIcon={Facebook} username="@react-stack" />
      <Social socialIcon={Instagram} username="@react_stack" />
    </section>

    <section className={Styles.section}>
      <h4 className={Styles['section-title']}>Menu</h4>

      <nav className={Styles.menu}>
        <ul className={Styles['menu-list']}>
          <li className={Styles['menu-item']}>
            <Link className={Styles['menu-link']} to="/">
              Home
            </Link>
          </li>
          <li className={Styles['menu-item']}>
            <Link className={Styles['menu-link']} to="/catalog">
              Catalog
            </Link>
          </li>
          <li className={Styles['menu-item']}>
            <Link className={Styles['menu-link']} to="/auth">
              Auth
            </Link>
          </li>
        </ul>
      </nav>
    </section>
  </footer>
);

export default memo(Footer);
