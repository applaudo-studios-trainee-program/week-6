import { FC, memo, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';

import { ISessionData } from 'services/entities/auth';
import { IUser } from 'models/userModels';
import Menu from 'assets/Icons/Menu';
import Styles from './Navbar.module.css';

type Props = {
  userData: IUser;
  setStorage: (
    value: ISessionData | ((val: ISessionData) => ISessionData)
  ) => void;
};

const Navbar: FC<Props> = ({ userData, setStorage }) => {
  const [show, setShow] = useState(false);

  const handleShow = () => setShow((state) => !state);

  const handleTagClick = () => setShow((state) => state && !state);

  const handleSignOut = () => {
    setStorage({
      jwt: '',
      user: { id: 0, username: '', firstName: '', lastName: '' },
    });

    setShow((state) => state && !state);
  };

  return (
    <nav className={Styles.container}>
      <Link to="/" onClick={handleTagClick}>
        <h3 className={Styles.title}>React Stack</h3>
      </Link>

      <ul
        className={
          show
            ? `${Styles['item-list']} ${Styles.show} animate__animated animate__fadeInRight`
            : `${Styles['item-list']} animate__animated animate__fadeInRight`
        }
      >
        <li className={Styles['list-item']}>
          <NavLink
            className={Styles['list-link']}
            to="/"
            onClick={handleTagClick}
          >
            Home
          </NavLink>
        </li>
        <li className={Styles['list-item']}>
          <NavLink
            className={Styles['list-link']}
            to="/catalog"
            onClick={handleTagClick}
          >
            Catalog
          </NavLink>
        </li>
        <li className={Styles['list-item']}>
          <NavLink
            className={Styles['list-link']}
            to="/search"
            onClick={handleTagClick}
          >
            Search
          </NavLink>
        </li>
        <li className={`${Styles['list-item']} ${Styles['user-data']}`}>
          <span className={Styles['user-content']}>
            <h3 className={Styles['user-name']}>
              Hello{' '}
              <small>
                {userData.username !== '' &&
                  `${userData.firstName} ${userData.lastName}`}
              </small>
            </h3>

            {userData.username !== '' ? (
              <button
                className={Styles['auth-btn']}
                type="button"
                onClick={handleSignOut}
              >
                Sign Out
              </button>
            ) : (
              <Link to="/auth" onClick={handleTagClick}>
                <button className={Styles['auth-btn']} type="button">
                  Sign In
                </button>
              </Link>
            )}
          </span>
        </li>
      </ul>

      <Menu className={Styles.icon} onClickFn={handleShow} />
    </nav>
  );
};

export default memo(Navbar);
