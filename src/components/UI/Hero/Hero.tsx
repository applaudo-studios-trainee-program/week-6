import { FC } from 'react';
import { Link } from 'react-router-dom';

import Styles from './Hero.module.css';

const Hero: FC = () => (
  <section className={Styles.container}>
    <h1 className={Styles.title}>React Stack</h1>
    <p className={Styles['sub-title']}>Your realiable game source</p>

    <Link className={Styles.link} to="/catalog">
      <h3>Full Catalog</h3>
    </Link>
  </section>
);

export default Hero;
