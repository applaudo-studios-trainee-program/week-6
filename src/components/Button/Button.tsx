import { FC, memo } from 'react';

import Styles from './Button.module.css';

type Props = {
  content: string;
  type: 'button' | 'submit';
  onClickFn?: () => void;
  disabled?: boolean;
};

const Button: FC<Props> = ({
  content,
  onClickFn,
  type = 'button',
  disabled = false,
}) => (
  <button
    className={Styles.button}
    disabled={disabled}
    onClick={onClickFn}
    type={type === 'button' ? 'button' : 'submit'}
  >
    {content}
  </button>
);

export default memo(Button);
