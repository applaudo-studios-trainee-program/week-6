import { FC, memo } from 'react';

import { IComment } from 'models/gamesModels';
import User from 'assets/Icons/User';
import Styles from './Comment.module.css';

const Comment: FC<IComment> = ({ body, user }) => {
  const { firstName, lastName } = user;

  return (
    <div className={Styles.container}>
      <div className={Styles.data}>
        <User className={Styles['data-icon']} />
        <p className={Styles['data-author']}>{`${firstName} ${lastName}`}</p>
      </div>

      <p className={Styles.content}>{body}</p>
    </div>
  );
};

export default memo(Comment);
