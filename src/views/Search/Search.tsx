import {
  FC,
  FormEvent,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import * as lodash from 'lodash';
import validator from 'validator';

import { getGamesBySearchQuery } from 'services/entities/games';
import { ISmallGamesData } from 'models/gamesModels';
import { useAuth } from 'hooks/useAuth';
import Button from 'components/Button/Button';
import SmallCard from 'components/SmallCard/SmallCard';
import Styles from './Search.module.css';

const Search: FC<RouteComponentProps> = ({ history, location }) => {
  const { search } = location;

  const normalizedQuery = search.replace(/[?q=]/g, '');

  const firstSearchRef = useRef(false);
  const isMountedRef = useRef(false);

  const [, setGames] = useState<ISmallGamesData[]>([]);
  const [actualPage, setActualPage] = useState(0);
  const [totalPages, setTotalPages] = useState(0);
  const [normalizedGameList, setNormalizedGameList] = useState<
    ISmallGamesData[]
  >([]);

  const [formState, handleChange] = useAuth({
    initialFormState: { query: decodeURI(normalizedQuery) },
  });

  const { query } = formState;

  useEffect(() => {
    isMountedRef.current = true;

    return () => {
      isMountedRef.current = false;
    };
  }, []);

  const debouncedRequest = useCallback(
    lodash.debounce(() => {
      history.push(`?q=${query}`);

      getGamesBySearchQuery({ searchQuery: query }).then((response) => {
        if (isMountedRef.current) {
          setGames(() => {
            setNormalizedGameList(() =>
              response.filter((_, index) => index < 6)
            );

            setTotalPages(Math.ceil(response.length / 6) - 1);

            setActualPage(0);

            return response;
          });
        }
      });
    }, 1000),
    [query]
  );

  const handleBackClick = useCallback(() => {
    setActualPage((actualPage) => {
      setGames((gameList) => {
        setNormalizedGameList(() =>
          gameList.filter(
            (_, index) =>
              index >= 6 * (actualPage - 1) && index < 6 * actualPage
          )
        );

        return gameList;
      });

      return actualPage - 1;
    });
  }, []);

  const handleNextClick = useCallback(() => {
    setActualPage((actualPage) => {
      setGames((gameList) => {
        setNormalizedGameList(() =>
          gameList.filter(
            (_, index) =>
              index >= 6 * (actualPage + 1) && index < 6 * (actualPage + 2)
          )
        );

        return gameList;
      });

      return actualPage + 1;
    });
  }, []);

  const handleSubmit = useCallback(
    (event: FormEvent<HTMLFormElement>) => {
      event.preventDefault();

      if (!validator.isEmpty(query.trim())) debouncedRequest();

      firstSearchRef.current = true;
    },
    [formState]
  );

  const gameList = useMemo(
    () =>
      normalizedGameList.map((game) => (
        <Link to={`game/${game.id}`} key={game.id}>
          <SmallCard {...game} />
        </Link>
      )),
    [normalizedGameList]
  );

  return (
    <div className={Styles.container}>
      <h1 className={Styles.title}>Search</h1>

      <form onSubmit={handleSubmit}>
        <label className={Styles['input-group']} htmlFor="query">
          Search for Games
          <input
            className={Styles.input}
            type="text"
            id="query"
            name="query"
            autoComplete="off"
            placeholder="E.g. Zelda"
            value={query}
            onChange={handleChange}
          />
        </label>

        <Button content="Search" type="submit" />
      </form>

      <section className={Styles['game-list']}>
        {!firstSearchRef.current ? (
          <p className={Styles['search-title']}>Make your first search 😄</p>
        ) : null}

        {firstSearchRef.current ? (
          gameList.length ? (
            gameList
          ) : (
            <p className={Styles['search-title']}>
              We couldn't find any game related 😔
            </p>
          )
        ) : null}
      </section>

      {totalPages ? (
        <div className={Styles.actions}>
          <Button
            content="Back"
            disabled={actualPage === 0}
            onClickFn={handleBackClick}
            type="button"
          />

          <Button
            content="Next"
            disabled={actualPage === totalPages}
            onClickFn={handleNextClick}
            type="button"
          />
        </div>
      ) : null}
    </div>
  );
};

export default withRouter(Search);
