import { FC, FormEvent, useEffect, useReducer, useRef, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import validator from 'validator';

import { addError, removeError } from 'actions/errorActions';
import { errorReducer } from 'reducers/errorReducer';
import { IErrorMessage, ISessionData, postUser } from 'services/entities/auth';
import { useAuth } from 'hooks/useAuth';
import Styles from './Auth.module.css';

type Props = {
  setStorage: (
    value: ISessionData | ((val: ISessionData) => ISessionData)
  ) => void;
};

const Auth: FC<Props & RouteComponentProps> = ({ history, setStorage }) => {
  const isMounted = useRef(false);

  const [formState, handleChange] = useAuth({
    initialFormState: {
      email: 'bmendoza.dev@hotmail.com',
      password: 'BC5rdP*kE%NV',
    },
  });

  const [errorOnResponse, setErrorOnResponse] = useState<IErrorMessage[]>([]);

  const { email, password } = formState;

  const [errorList, errorDispatch] = useReducer(errorReducer, {});

  useEffect(() => {
    if (isMounted.current) {
      if (!Object.keys(errorList).length) {
        postUser({ identifier: email, password }).then((response) => {
          if (response.error) {
            setErrorOnResponse(response.errorMessages || []);
          } else if (isMounted.current) {
            const { error, ...rest } = response;

            setStorage(rest as ISessionData);
            setErrorOnResponse([]);

            history.push('/');
          }
        });
      }
    }
  }, [errorList]);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  }, []);

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!validator.isEmpty(email.trim())) {
      if (!validator.isEmail(email.trim())) {
        errorDispatch(
          addError({
            inputName: 'email',
            message: 'Please type a valid email.',
          })
        );
      } else errorDispatch(removeError({ inputName: 'email' }));
    } else {
      errorDispatch(
        addError({
          inputName: 'email',
          message: "This input can't be empty.",
        })
      );
    }

    if (!validator.isEmpty(password.trim())) {
      if (!validator.isLength(password.trim(), { min: 6 })) {
        errorDispatch(
          addError({
            inputName: 'password',
            message: 'Passwords must be at least six characters long.',
          })
        );
      } else errorDispatch(removeError({ inputName: 'password' }));
    } else {
      errorDispatch(
        addError({
          inputName: 'password',
          message: "This input can't be empty.",
        })
      );
    }
  };

  return (
    <div className={Styles.container}>
      <h1 className={Styles.title}>Sign In</h1>

      {errorOnResponse.map((errorMessage) => (
        <p className={Styles['error-on-request']} key={errorMessage.id}>
          <small>{errorMessage.message}</small>
        </p>
      ))}

      <form className={Styles.form} onSubmit={handleSubmit}>
        <label className={Styles['input-group']} htmlFor="email">
          Email
          <input
            className={Styles.input}
            type="text"
            id="email"
            name="email"
            autoComplete="off"
            placeholder="john.doe@example.com"
            value={email}
            onChange={handleChange}
          />
          {errorList.email && (
            <p className={Styles['error-message']}>
              <small>{errorList.email.message}</small>
            </p>
          )}
        </label>

        <label className={Styles['input-group']} htmlFor="password">
          Password
          <input
            className={Styles.input}
            type="password"
            id="password"
            name="password"
            value={password}
            onChange={handleChange}
          />
          {errorList.password && (
            <p className={Styles['error-message']}>
              <small>{errorList.password.message}</small>
            </p>
          )}
        </label>

        <button className={Styles['form-btn']} type="submit">
          Sign In
        </button>
      </form>
    </div>
  );
};

export default withRouter(Auth);
