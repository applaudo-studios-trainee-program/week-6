import { FC, useCallback } from 'react';
import { Link } from 'react-router-dom';

import { usePagination } from 'hooks/usePagination';
import Button from 'components/Button/Button';
import Card from 'components/Card/Card';
import Spinner from 'assets/Icons/Spinner/Spinner';
import Styles from './Catalog.module.css';

const Catalog: FC = () => {
  const { itemsToShow, actualPage, totalPages, setActualPage } = usePagination({
    itemsPerPage: 8,
    searchEndpoint: '&_sort=name',
  });

  const handleBackClick = useCallback(() => {
    setActualPage((state) => state - 1);
  }, []);

  const handleNextClick = useCallback(() => {
    setActualPage((state) => state + 1);
  }, []);

  return (
    <div className={Styles.container}>
      <section className={Styles.list}>
        {itemsToShow.length ? (
          itemsToShow.map((item) => (
            <Link key={item.id} to={`game/${item.id}`}>
              <Card {...item} />
            </Link>
          ))
        ) : (
          <Spinner />
        )}
      </section>

      <div className={Styles.actions}>
        <Button
          content="Back"
          disabled={actualPage === 0}
          onClickFn={handleBackClick}
          type="button"
        />

        <Button
          content="Next"
          disabled={actualPage === totalPages}
          onClickFn={handleNextClick}
          type="button"
        />
      </div>
    </div>
  );
};

export default Catalog;
