import { FC, useEffect, useRef, useState } from 'react';
import { useParams } from 'react-router-dom';

import { getGameById, getGameComments } from 'services/entities/games';
import { IComment, ICoverArt, IMetaInfo } from 'models/gamesModels';
import { ISessionData } from 'services/entities/auth';
import { postComment } from 'services/entities/comments';
import { useLocalStorage } from 'hooks/useLocalStorage';
import Comment from 'components/Comment/Comment';
import CommentMaker from 'components/CommentMaker/CommentMaker';
import MetaInfo from 'components/MetaInfo/MetaInfo';
import Spinner from 'assets/Icons/Spinner/Spinner';
import Styles from './Game.module.css';

type GameData = {
  name: string;
  coverArt: ICoverArt;
  metaInfo: IMetaInfo[];
};

const Game: FC = () => {
  const { id } = useParams<{ id: string }>();

  const isMounted = useRef(false);

  const { storedValue } = useLocalStorage<ISessionData>({
    key: 'sessionData',
    initialValue: {
      jwt: '',
      user: { id: 0, username: '', firstName: '', lastName: '' },
    },
  });

  const [gameData, setGameData] = useState<{} | GameData>({});
  const [comments, setComments] = useState<IComment[]>([]);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  }, []);

  useEffect(() => {
    getGameById({ gameId: id }).then((response) => {
      if (isMounted.current) setGameData(response);
    });

    getGameComments({ gameId: id }).then((response) => {
      if (isMounted.current) setComments(response);
    });
  }, []);

  if (!Object.keys(gameData).length) return <Spinner />;

  const { name, coverArt, metaInfo } = gameData as GameData;

  const { url } = coverArt;

  const handleCommentSubmit = ({ comment }: { comment: string }) => {
    const { jwt, user } = storedValue;

    postComment({ body: comment, gameId: id, token: jwt });
    setComments((state) => [
      ...state,
      { body: comment, user, id: `${Math.random()}` },
    ]);
  };

  return (
    <div className={Styles.container}>
      <h1 className={Styles.title}>
        <small>{name}</small>
      </h1>

      <img
        className={Styles.image}
        src={url}
        alt={`${name} Cover`}
        title={`${name} Cover`}
      />

      <section className={Styles['meta-container']}>
        {metaInfo.map((metaData) => (
          <MetaInfo key={metaData.id} {...metaData} />
        ))}
      </section>

      <section className={Styles['comment-list']}>
        <h2 className={Styles['comment-list-title']}>Comments</h2>

        {comments.length ? (
          comments.map((comment) => <Comment key={comment.id} {...comment} />)
        ) : (
          <p style={{ margin: 0 }}>There are no comments yet 😮</p>
        )}
      </section>

      <section className={Styles['comment-maker']}>
        <CommentMaker onSubmitFn={handleCommentSubmit} />
      </section>
    </div>
  );
};
export default Game;
