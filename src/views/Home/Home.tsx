import { FC, useEffect, useRef, useState } from 'react';
import { Link } from 'react-router-dom';

import { getGames } from 'services/entities/games';
import { IGamesData } from 'models/gamesModels';
import Card from 'components/Card/Card';
import Hero from 'components/UI/Hero/Hero';
import Styles from './Home.module.css';

const Home: FC = () => {
  const isMountedRef = useRef(false);

  const [recentlyUpdated, setRecentlyUpdated] = useState<IGamesData[]>([]);
  const [newReleases, setNewReleases] = useState<IGamesData[]>([]);

  useEffect(() => {
    isMountedRef.current = true;

    return () => {
      isMountedRef.current = false;
    };
  }, []);

  useEffect(() => {
    getGames({ offset: 4, searchEndpoint: '&_sort=created_at' }).then(
      (response) => {
        if (isMountedRef.current) setNewReleases(response);
      }
    );

    getGames({ offset: 4, searchEndpoint: '&_sort=updated_at' }).then(
      (response) => {
        if (isMountedRef.current) setRecentlyUpdated(response);
      }
    );
  }, []);

  return (
    <>
      <Hero />

      <section className={Styles['game-section']}>
        <h2 className={Styles['section-title']}>Recently Added</h2>

        {newReleases.map((gameData) => (
          <Link to={`game/${gameData.id}`} key={gameData.id}>
            <Card {...gameData} />
          </Link>
        ))}
      </section>

      <section className={Styles['game-section']}>
        <h2 className={Styles['section-title']}>Recently Updated</h2>

        {recentlyUpdated.map((gameData) => (
          <Link to={`game/${gameData.id}`} key={gameData.id}>
            <Card {...gameData} />
          </Link>
        ))}
      </section>
    </>
  );
};

export default Home;
